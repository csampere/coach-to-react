# What is this application?

This is a Number Facts Application , it will convert a number from the input to Roman Numerals , Fizz Buzz and grab a fun trivia fact for a specific number.

## Why?

This was made for some ReactJS coaching sessions between myself ( Clayton Sampere) and Ruseell Wind

## Setup

1. Firstly clone and follow the instructions on the mvf-reverse-proxy repo : `https://bitbucket.org/mvfglobal/mvf-reverse-proxy/src/master/`
2. Clone the repo by typing `Git clone git@bitbucket.org:csampere/coach-to-react.git` via your termial
3. Then in the terminal type `cd coach-to-react`
4. Next type `make` and hit enter and the application should build and compile.
5. Once that is done finally go on your favourite browser (mine is Chrome) and enter the url`http://number-facts.localhost:2000/`

## Available Scripts

In the project directory, you can run:

### `make`

This runs the default command which in this instance it is `build` , its recommonded to just run this on your first time.

### `make build`

This creates the containers and network and runs the dockerized application

### `make rebuild`

Re-makes the docker containers and re-creates the network if its not already there.

### `make up`

This starts the application and starts up the containers that have been built previously

### `make down`

Kills and stops all containers and the container network

### `make shell`

connects to the docker container and brings you to the shell to enable you to run shell commands on the container if needed.

### `make test`

This runs our applications tests which are under the __test__ folders under /src

### `make logs`

This shows the logs from the container up to 100 lines.