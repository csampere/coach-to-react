FROM node:18-alpine AS runner

ENV SHELL=/bin/bash

ARG OS_UID
ARG OS_GID

RUN apk add --no-cache bash curl shadow

USER root
RUN deluser --remove-home node
RUN /usr/sbin/groupadd -g ${OS_GID} www
RUN /usr/sbin/useradd -s /bin/bash -m -g ${OS_GID} -u ${OS_UID} www

RUN mkdir /app && chown www:www /app
RUN mkdir /dist && chown www:www /dist

USER www

ENV NPM_CONFIG_PREFIX="/home/www/.npm"
ENV PATH="/home/www/.npm/bin:${PATH}"

WORKDIR /app
COPY --chown=www package*.json ./
RUN npm install

COPY --chown=www:www   . .

EXPOSE 3000
