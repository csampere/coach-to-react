.DEFAULT_GOAL := build

SETUP := $(shell ls -a ./ | grep -q .env.docker || ./scripts/setup.sh)
include .env.docker

.PHONY: build
build: down rebuild up

.PHONY: rebuild
rebuild:
	docker-compose build --pull --no-cache

.PHONY: up
up:
	$(MAKE) down
	docker network create mvf_shared || true
	docker-compose up -d
	$(MAKE) logs

.PHONY: down
down:
	docker-compose down

.PHONY: shell
shell:
	docker exec -itu www number-facts sh

.PHONY: test
test:
	docker exec -itu www number-facts npm test -- -t $(filter)

.PHONY: logs
logs:
	docker-compose logs -f --tail=100
