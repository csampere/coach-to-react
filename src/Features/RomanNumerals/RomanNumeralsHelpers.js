export const convertToRomanNumerals = (count) => {
    const romanNumberValues = {
        CM : 900,
        D : 500,
        CD : 400,
        C : 100,
        XC : 90,
        L : 50,
        XL : 40,
        X : 10,
        IX : 9,
        V : 5,
        IV : 4,
        I : 1,
    } 
    let romanNumeralString = ''
    if ( count < 0){
        romanNumeralString += "-"
        count = Math.abs(count)
    }
    for (const [romanNumeralLetter, integerValue] of Object.entries(romanNumberValues)) {
        while ( count >= integerValue ) {
            romanNumeralString += romanNumeralLetter;
            count -= integerValue;
        }
    }
    return romanNumeralString
}