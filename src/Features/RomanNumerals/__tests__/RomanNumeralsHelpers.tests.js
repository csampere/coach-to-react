import {convertToRomanNumerals} from '../RomanNumeralsHelpers'

describe('RomanNumeralsHelpersTests', () => {
    it('should return I when passed 1', () => {
        expect(convertToRomanNumerals(1)).toEqual('I')
    })
    it('should return II when passed 2', () => {
        expect(convertToRomanNumerals(2)).toEqual('II')
    })
    it('should return III when passed 3', () => {
        expect(convertToRomanNumerals(3)).toEqual('III')
    })
    it('should return IV when passed 4', () => {
        expect(convertToRomanNumerals(4)).toEqual('IV')
    })
    it('should return V when passed 5', () => {
        expect(convertToRomanNumerals(5)).toEqual('V')
    })
    it('should return VI when passed 6', () => {
        expect(convertToRomanNumerals(6)).toEqual('VI')
    })
    it('should return VII when passed 7', () => {
        expect(convertToRomanNumerals(7)).toEqual('VII')
    })
    it('should return VIII when passed 8', () => {
        expect(convertToRomanNumerals(8)).toEqual('VIII')
    })
    it('should return IX when passed 9', () => {
        expect(convertToRomanNumerals(9)).toEqual('IX')
    })
    it('should return X when passed 10', () => {
        expect(convertToRomanNumerals(10)).toEqual('X')
    })
    it('should return XX when passed 20', () => {
        expect(convertToRomanNumerals(20)).toEqual('XX')
    })
    it('should return XXX when passed 30', () => {
        expect(convertToRomanNumerals(30)).toEqual('XXX')
    })
    it('should return XL when passed 40', () => {
        expect(convertToRomanNumerals(40)).toEqual('XL')
    })
    it('should return L when passed 50', () => {
        expect(convertToRomanNumerals(50)).toEqual('L')
    })
    it('should return LX when passed 60', () => {
        expect(convertToRomanNumerals(60)).toEqual('LX')
    })
    it('should return LXX when passed 70', () => {
        expect(convertToRomanNumerals(70)).toEqual('LXX')
    })
    it('should return LXXX when passed 80', () => {
        expect(convertToRomanNumerals(80)).toEqual('LXXX')
    })
    it('should return XC when passed 90', () => {
        expect(convertToRomanNumerals(90)).toEqual('XC')
    })
    it('should return C when passed 100', () => {
        expect(convertToRomanNumerals(100)).toEqual('C')
    })
    it('should return CC when passed 200', () => {
        expect(convertToRomanNumerals(200)).toEqual('CC')
    })
    it('should return CC when passed 300', () => {
        expect(convertToRomanNumerals(300)).toEqual('CCC')
    })
    it('should return CD when passed 400', () => {
        expect(convertToRomanNumerals(400)).toEqual('CD')
    })
    it('should return D when passed 500', () => {
        expect(convertToRomanNumerals(500)).toEqual('D')
    })
    it('should return DC when passed 600', () => {
        expect(convertToRomanNumerals(600)).toEqual('DC')
    })
    it('should return DCC when passed 700', () => {
        expect(convertToRomanNumerals(700)).toEqual('DCC')
    })
    it('should return DCCC when passed 800', () => {
        expect(convertToRomanNumerals(800)).toEqual('DCCC')
    })
    it('should return CM when passed 900', () => {
        expect(convertToRomanNumerals(900)).toEqual('CM')
    })
    it('should return -I when passed -1', () => {
        expect(convertToRomanNumerals(-1)).toEqual('-I')
    })

} )