import React from 'react'
import {render, fireEvent, screen} from '@testing-library/react'
import NumberFacts from '../NumberFacts'

describe('NumberFacts', () => {
    it("Should view Roman Numeral value of a number", () => {
       render(<NumberFacts/>);

       const selectFactTypeInput = screen.getByRole("combobox")
       fireEvent.change(selectFactTypeInput, {target:{value: "RomanNumeral"}})
       const changeInputValue = screen.getByRole("spinbutton")
       fireEvent.change(changeInputValue, {target: {value: "2"}})

       const numberFactsOutput = screen.getByText('II')
       expect(numberFactsOutput).toBeTruthy()
       const numberFactsTypeOutputElement = screen.getByText('Roman Numeral:')
       expect(numberFactsTypeOutputElement).toBeTruthy()
    })

    it("Should view fizzBuzz with a value of 15", () => {
        render(<NumberFacts/>);
 
        const selectFactTypeInput = screen.getByRole("combobox")
        fireEvent.change(selectFactTypeInput, {target:{value: "FizzBuzz"}})
        const changeInputValue = screen.getByRole("spinbutton")
        fireEvent.change(changeInputValue, {target: {value: "15"}})
 
        const numberFactsOutputElement = screen.getByText('FizzBuzz')
        expect(numberFactsOutputElement).toBeTruthy()
        const numberFactsTypeOutputElement = screen.getByText('FizzBuzz:')
        expect(numberFactsTypeOutputElement).toBeTruthy()
     })
})