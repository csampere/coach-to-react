import { useTheme } from "@mui/material";

const useOutputFactStyles = () => {
    const theme = useTheme();
    return {
      loader: {
        color: theme.palette.secondary.main,
      },
    }
  }

  export default useOutputFactStyles;