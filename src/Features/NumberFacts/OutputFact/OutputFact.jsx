import {fizzbuzz, convertToRomanNumerals, getTrivia} from './OutputFactHelpers'
import React from 'react'
import { useNumberFacts } from '../NumberFactsState/NumberFactsState'
import {CircularProgress} from '@mui/material'
import useOutputFactStyles from './UseOutputFactStyles'

const facts = {
    FizzBuzz: {function: fizzbuzz, message:"FizzBuzz:"},
    RomanNumeral: {function: convertToRomanNumerals, message:"Roman Numeral:"},
    Trivia: {function: getTrivia, message:"Trivia:"}
}

const OutputFact = () => {
    const [state, ] = useNumberFacts()
    const {factType, count} = state
    const fact = facts[factType]
    const[factString, setFactString] = React.useState();
    const styles = useOutputFactStyles()
    
    
    React.useEffect(() => {
        const asyncSetFactFunction = async () => {
                try {
                    setFactString(<CircularProgress sx={styles.loader} />)
                    const newFactString = await getTrivia(count);
                    setFactString(newFactString)
                } catch (error) {
                    console.log("unable to change fact")
                }
        }
        if (factType === "Trivia") {
            asyncSetFactFunction()
        } else {
            setFactString(fact.function(count))
        }
        
    },[fact,factType,count])

    return (
    <>
        <h2><strong>{fact.message}</strong><span> </span> {factString}</h2>
    </>
    )
}
export default OutputFact