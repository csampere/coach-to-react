export const getTrivia = async (count) => {
    const url = `http://numbersapi.com/${count}/trivia`

    try {
        const response = await fetch(url);
        return response.text()
    } catch (error){
        return "Failed to get Fact"
    }
    
}

export const fizzbuzz = function(count){
    if ( count % 3 === 0 && count % 5 === 0 ){
        return "FizzBuzz"
    } else if ( count % 5 === 0){
        return "Buzz"
    } else if( count % 3 === 0){
        return "Fizz"
    } else {
        return String(count)
    }

}

export const convertToRomanNumerals = (count) => {
    const romanNumberValues = {
        CM : 900,
        D : 500,
        CD : 400,
        C : 100,
        XC : 90,
        L : 50,
        XL : 40,
        X : 10,
        IX : 9,
        V : 5,
        IV : 4,
        I : 1,
    } 
    let romanNumeralString = ''
    if ( count < 0){
        romanNumeralString += "-"
        count = Math.abs(count)
    }
    for (const [romanNumeralLetter, integerValue] of Object.entries(romanNumberValues)) {
        while ( count >= integerValue ) {
            romanNumeralString += romanNumeralLetter;
            count -= integerValue;
        }
    }
    return romanNumeralString
}