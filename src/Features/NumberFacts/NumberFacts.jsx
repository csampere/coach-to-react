import React from 'react'
import { NumberFactsProvider } from './NumberFactsState/NumberFactsState'
import NumberInput from './NumberInput/NumberInput'
import OutputFact from './OutputFact/OutputFact'
import SelectFactType from './SelectFactType/SelectFactType'
import { Grid, Card, CardContent} from '@mui/material'

const NumberFacts = () => {
    
    return (
        <NumberFactsProvider>
            <Card sx={{ Height: '50%', mx: "auto", width: '50%' }}>
                <CardContent>
                <Grid container direction="column" alignItems="center" mt={3} spacing={2}>
                    <Grid item>
                        <Grid container direction="row" spacing={2}>
                            <Grid item>
                                <NumberInput />
                                </Grid>
                            <Grid item>
                                <SelectFactType />
                                </Grid>
                        </Grid>
                        </Grid>
                        <Grid item>
                            <OutputFact />
                        </Grid>
                        
                    <div id="card">
                    
                    {/* Select Fact Type and its properties go here */}
                    
                    </div>
                </Grid>
                </CardContent>
                </Card>
            
        </NumberFactsProvider>
    )
}

export default NumberFacts