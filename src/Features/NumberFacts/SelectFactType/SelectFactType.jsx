import { changeFactType } from "../NumberFactsState/NumberFactsActions"
import { useNumberFacts } from "../NumberFactsState/NumberFactsState"
import { FormControl, Select, MenuItem, InputLabel } from "@mui/material"

const SelectFactType = () => {
    const [state, dispatch] = useNumberFacts()
    const {factType} = state

    const handleChange = (e) => {
        dispatch(changeFactType(e.target.value))
    }
    return (
        <FormControl fullWidth>
            <InputLabel id="factTypeLabel">Fact Type</InputLabel>
            <Select
                labelId="factTypeLabel"
                id="factTypeSelect"
                value={factType}
                label="FactType"
                onChange={handleChange}
                fullWidth
            >
            <MenuItem value="RomanNumeral">Roman Numeral</MenuItem>
            <MenuItem value="FizzBuzz">FizzBuzz</MenuItem>
            <MenuItem value="Trivia">Trivia</MenuItem>
            </Select>
        </FormControl>
    )
}
export default SelectFactType