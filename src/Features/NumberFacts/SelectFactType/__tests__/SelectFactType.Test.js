import React from 'react'
import {render, fireEvent, screen} from '@testing-library/react'
import SelectFactType from '../SelectFactType'
import { NumberFactsProvider } from '../../NumberFactsState/NumberFactsState';
import * as NumberFactsActions from '../../NumberFactsState/NumberFactsActions';


describe('<SelectFactType />', () => {
    it("should trigger onChange callback when option is selected.", () => {
      const changeFactTypeMock = jest.spyOn(NumberFactsActions, 'changeFactType');
      render(
        <NumberFactsProvider>
          <SelectFactType />
        </NumberFactsProvider>,
      );
      const selectFactTypeInput = screen.getByRole("combobox")
      fireEvent.change(selectFactTypeInput, {target: {value: "RomanNumeral"}})
      expect(changeFactTypeMock).toHaveBeenCalled()
    });
  });