import { useNumberFacts } from "../NumberFactsState/NumberFactsState"
import { TextField } from "@mui/material"

const NumberInput = () => {
    const [state, dispatch] = useNumberFacts()
    const {count} = state

    const handleChange = (e) => {
        dispatch({type: "count", count: e.target.value})
    }
    return <TextField id="numberInput" label="Number" type='number' value={count} onChange={handleChange}/>
}
export default NumberInput
