import React from 'react'
// Not gone over createContext before
const NumberFactsContext = React.createContext()
// set inital state to fizzbuzz
export const initialState = {
  factType: 'FizzBuzz',
  count: '1'
}
// Changes the state to the action parameter
const numberFactsReducer = (state, action) => {
  switch (action.type) {
    case 'factType': {
      const {factType} = action
      return {
        ...state,
        factType: factType
      }
    }
    case 'count': {
        const {count} = action
        return {
          ...state,
          count: count
        }
      }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`)
    }
  }
}

// sets the state to the initial state and returns NumberfactsContext
export const NumberFactsProvider = function({children}) {
    // dispatch({type: "factType", factType: "romanNumeral"}) triggers a action to update fact type to roman numeral.
  const [state, dispatch] = React.useReducer(numberFactsReducer, initialState)
  return <NumberFactsContext.Provider value={[state, dispatch]}>{children}</NumberFactsContext.Provider>
}

export const useNumberFacts = function() {
  const context = React.useContext(NumberFactsContext)
  if (context === undefined) {
    throw new Error(`useNumberFacts must be used within a NumberFactsProvider`)
  }
  return context
}