export const changeFactType = (factType) => {
    return {
        type: 'factType',
        factType
    }
}