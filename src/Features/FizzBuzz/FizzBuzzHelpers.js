export const fizzbuzz = function(count){
    if ( count % 3 === 0 && count % 5 === 0 ){
        return "FizzBuzz"
    } else if ( count % 5 === 0){
        return "Buzz"
    } else if( count % 3 === 0){
        return "Fizz"
    } else {
        return String(count)
    }

}

